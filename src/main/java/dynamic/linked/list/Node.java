/*
 * A node for a dynamically linked list
 */
package dynamic.linked.list;

public class Node {
    Node prev;
    Node next;
    int data;

    public Node(int data) {
        this.data = data;
    }
}
