package dynamic.linked.list;

import sun.util.resources.cldr.ext.CurrencyNames_ebu;

public class DynamicallyLinkedList {
    Node head;

    public void append(int data) {
        if (head == null) {
            head = new Node(data);
            return;
        }

        Node currentHead = head;
        while (currentHead.next != null) {
            currentHead = currentHead.next;
        }
        currentHead.next = new Node(data);
        currentHead.next.prev = currentHead;

    }

    public void prepend(int data) {
        if (head == null) {
            head = new Node(data);
            return;
        }
        Node newHead = new Node(data);
        head.prev = newHead;
        newHead.next = head;
        head = newHead;

    }

    public void deleteWithValue(int data) {
        if (head.data == data) {
            head = head.next;
            head.prev = null;
            return;
        }

        Node current = head;
        while (current.next.data != data) {
            current = current.next;
        }

        if (current.next.next == null) {
            current.next = null;
            return;
        }
        current.next = current.next.next;
        current.next.prev = current;

    }
}
